#include "bin_manual.h"

BinarizationManual::BinarizationManual(PNM* img) :
    Transformation(img)
{
}

BinarizationManual::BinarizationManual(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationManual::transform()
{
    int threshold = getParameter("threshold").toInt();

    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    if (image->format() == QImage::Format_RGB32) {
        ConversionGrayscale cg(image);
        image = cg.transform();
    }
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++) {
            QColor pixel = image->pixel(x,y);
            int newValue = pixel.value() < threshold ? 0 : 1;
            newImage->setPixel(x, y, newValue);
        }

    return newImage;
}





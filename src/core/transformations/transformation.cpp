#include "transformation.h"

Transformation::Transformation(PNM* image) :
    QThread(0)
{
    parameters = new QHash<QString, QVariant>;
    this->image = image;
    this->supervisor = 0;
}

Transformation::Transformation(PNM* image, ImageViewer* iv) :
    QThread(0)
{
    parameters = new QHash<QString, QVariant>;
    this->image = image;
    this->supervisor = iv;

    if (iv)
    {
        connect(this, SIGNAL(started()), iv, SLOT(transformationStarted()));
        connect(this, SIGNAL(finished()), iv, SLOT(transformationFinished()));
        connect(this, SIGNAL(message(QString)), iv, SLOT(toolMessage(QString)));
        connect(this, SIGNAL(progress(int)), iv, SLOT(toolProgress(int)));
        connect(this, SIGNAL(resultReady(PNM*)), iv, SLOT(updateImage(PNM*)));
        connect(this, SIGNAL(finished()), iv, SLOT(toolFinished()));
    }
}

Transformation::~Transformation()
{
    delete parameters;
    if (supervisor)
    {
        disconnect(this, SIGNAL(started()));
        disconnect(this, SIGNAL(message(QString)));
        disconnect(this, SIGNAL(progress(int)));
        disconnect(this, SIGNAL(resultReady(PNM*)));
        disconnect(this, SIGNAL(finished()));
    }
}

/** Sets a parameter param of the transformation
* with the value.
* If the parameter was already set it is overridden
* Returns the pointer to the Transformation object so it can be used as a factory.
*/
Transformation* Transformation::setParameter(QString param, QVariant value)
{
    parameters->insert(param, value);
    return this;
}

/** Returns the value of the given param.
* If the param isn't set returns a default QVariant()
* which is an invalid variant
* @see QVariant::QVariant()
*/
QVariant Transformation::getParameter(QString param)
{
    return parameters->value(param, QVariant());
}

void Transformation::run()
{
    PNM* image = this->transform();
    emit resultReady(image);
}

/**
* /virtual, abstract!/
* Virtual method that do some transformations (based on parameter Hash)
* in the Transformation class it returns a pointer to
* the loaded image or a new null PNM image
*/
PNM* Transformation::transform()
{
    if (image)
        return image;
    else
        return new PNM();
}

/** Returns a pointer to the image stored in the class */
PNM* Transformation::getImage()
{
    return image;
}

/** Returns a pixel value at given coordinates using different modes */
QRgb Transformation::getPixel(int x, int y, Mode mode)
{
    switch (mode) {
    case CyclicEdge:    return getPixelCyclic(x,y);
    case NullEdge:      return getPixelNull(x,y);
    case RepeatEdge:    return getPixelRepeat(x,y);
    default:            return image->pixel(x,y);
    }

}

/** Returns a pixel using the Cyclic mode:
 *  pixel(x,y) = pixel(x%width, y%width);
 */
QRgb Transformation::getPixelCyclic(int x, int y)
{
    int width = image->width();
    int height = image->height();

    return image->pixel(x%width, y%height);
}

/**
  * Returns a given pixel
  * If the pixel is out of image boundaries Black is returned;
  */
QRgb Transformation::getPixelNull(int x, int y)
{
    int width = image->width();
    int height = image->height();

    if (isPixelOutside(x, y, width, height)) {
        QColor pixel = QColor(0,0,0);
        return pixel.rgb();
    }

    return image->pixel(x,y);
}

/**
  * Returns given pixel.
  * If the pixel is out of image boundaries
  * the nearest edge pixel is given
  */
QRgb Transformation::getPixelRepeat(int x, int y)
{
    int width = image->width();
    int height = image->height();

    if (!isPixelOutside(x, y, width, height)) {
        return image->pixel(x,y);
    }
    else {
        if (isPixelOnBottomLeftCorner(x,y,height)) {
            return image->pixel(0, height-1);
        }
        if (isPixelOnBottomRightCorner(x,y,width,height)) {
            return image->pixel(width-1, height-1);
        }
        if (isPixelOnTopLeftCorner(x,y)) {
            return image->pixel(0, 0);
        }
        if (isPixelOnTopRightCorner(x,y,width)) {
            return image->pixel(width-1, 0);
        }
        if (isPixelOnBottom(y,height)) {
            return image->pixel(x, height-1);
        }
        if (isPixelOnTop(y)) {
            return image->pixel(x, 0);
        }
        if (isPixelOnLeft(x)) {
            return image->pixel(0, y);
        }
        if (isPixelOnRight(x, width)) {
            return image->pixel(width-1, y);
        }
    }
}

/** Returns a size x size part of the image centered around (x,y) */
math::matrix<float> Transformation::getWindow(int x, int y, int size,
                                              Channel channel,
                                              Mode mode = RepeatEdge)
{
    math::matrix<float> window(size,size);

    int radius = size/2;

    if (channel == LChannel) {
        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++) {
                QColor color = QColor::fromRgb(getPixel(x+j-radius, y+i-radius, mode));
                window[i][j] = color.value();
            }
    }
    else {

        int (* windowPixelFunction) (QRgb);

        switch (channel) {
        case RChannel:
            windowPixelFunction = qRed;
            break;
        case GChannel:
            windowPixelFunction = qGreen;
            break;
        case BChannel:
            windowPixelFunction = qBlue;
            break;
        }

        for (int i=0; i<size; i++)
            for (int j=0; j<size; j++) {
                QRgb rgb = getPixel(x+j-radius, y+i-radius, mode);
                float a = (float) windowPixelFunction(rgb);
                window[i][j] = a;
            }
    }
    return window;
}

ImageViewer* Transformation::getSupervisor()
{
    return supervisor;
}

bool Transformation::isPixelOutside(int x, int y, int width, int height) {
    return x>=width || x<0 || y>=height || y<0;
}


bool Transformation::isPixelOnLeft(int x) {
    return x < 0;
}

bool Transformation::isPixelOnRight(int x, int width) {
    return x >= width;
}

bool Transformation::isPixelOnTop(int y) {
    return y < 0;
}

bool Transformation::isPixelOnBottom(int y, int height) {
    return y >= height;
}

bool Transformation::isPixelOnTopLeftCorner(int x, int y) {
    return isPixelOnTop(y) && isPixelOnLeft(x);
}

bool Transformation::isPixelOnBottomLeftCorner(int x, int y, int height) {
    return isPixelOnBottom(y, height) && isPixelOnLeft(x);
}

bool Transformation::isPixelOnTopRightCorner(int x, int y, int width) {
    return isPixelOnTop(y) && isPixelOnRight(x, width);
}

bool Transformation::isPixelOnBottomRightCorner(int x, int y, int width, int height) {
    return isPixelOnBottom(y, height) && isPixelOnRight(x, width);
}

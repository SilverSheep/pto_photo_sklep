#include "map_normal.h"

#include "edge_sobel.h"
#include "map_height.h"

MapNormal::MapNormal(PNM* img) :
    Convolution(img)
{
}

MapNormal::MapNormal(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* MapNormal::transform()
{
    int width  = image->width(),
        height = image->height();

    double strength = getParameter("strength").toDouble();

    PNM* newImage = new PNM(width, height, image->format());

    MapHeight mh(image);
    image = mh.transform();

    EdgeSobel es(image);
    math::matrix<float>* gradientX = es.rawHorizontalDetection();
    math::matrix<float>* gradientY = es.rawVerticalDetection();

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            std::vector<float> d;
            d.resize(3);

            d[0] = (*gradientX)[x][y]/255.0f;
            d[1] = (*gradientY)[x][y]/255.0f;
            d[2] = 1/strength;

            normalizeAndScale(d);

            QColor newPixel = QColor(d[0], d[1], d[2]);
            newImage->setPixel(x, y, newPixel.rgb());
        }

    delete gradientX;
    delete gradientY;

    return newImage;
}

void MapNormal::normalizeAndScale(std::vector<float>& vector)
{
    float sum = 0;

    for (int i=0; i<vector.size(); ++i) {
        sum += vector[i]*vector[i];
    }

    float length = sqrt(sum);

    for (int i=0; i<vector.size(); ++i) {
        vector[i] = ((vector[i]/length)+1)*255/2;
    }
}

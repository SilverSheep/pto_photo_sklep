#include "edge_gradient.h"

EdgeGradient::EdgeGradient(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

EdgeGradient::EdgeGradient(PNM* img) :
    Convolution(img)
{
}

PNM* EdgeGradient::verticalDetection()
{
    return convolute(g_y, RepeatEdge);
}

PNM* EdgeGradient::horizontalDetection()
{
    return convolute(g_x, RepeatEdge);
}

PNM* EdgeGradient::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    PNM* imageX = new PNM(width, height, image->format());
    PNM* imageY = new PNM(width, height, image->format());

    imageX = horizontalDetection();
    imageY = verticalDetection();

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            if (image->format() == QImage::Format_Indexed8) {
                QColor imgX = imageX->pixel(x,y);
                QColor imgY = imageY->pixel(x,y);

                float newValue = qMin(sqrt(imgX.value()*imgX.value() + imgY.value()*imgY.value()), (double)PIXEL_VAL_MAX);
                newImage->setPixel(x,y,newValue);
            }
            else { //if (image->format() == QImage::Format_RGB32)
                QRgb imgX = imageX->pixel(x,y);
                QRgb imgY = imageY->pixel(x,y);

                int rX = qRed(imgX);
                int gX = qGreen(imgX);
                int bX = qBlue(imgX);
                int rY = qRed(imgY);
                int gY = qGreen(imgY);
                int bY = qBlue(imgY);

                int newR = qMin(sqrt(rX*rX + rY*rY), (double)PIXEL_VAL_MAX);
                int newG = qMin(sqrt(gX*gX + gY*gY), (double)PIXEL_VAL_MAX);
                int newB = qMin(sqrt(bX*bX + bY*bY), (double)PIXEL_VAL_MAX);

                QColor newPixel = QColor(newR, newG, newB);
                newImage->setPixel(x,y, newPixel.rgb());
            }
        }

    delete imageX;
    delete imageY;

    return newImage;
}


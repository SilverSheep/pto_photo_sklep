#include "noise_median.h"

NoiseMedian::NoiseMedian(PNM* img) :
    Convolution(img)
{
}

NoiseMedian::NoiseMedian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseMedian::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8) {
        // Iterate over image space
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                newImage->setPixel(x, y, getMedian(x, y, LChannel));
            }
    }
    else {
        // Iterate over image space
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int r = getMedian(x, y, RChannel);
                int g = getMedian(x, y, GChannel);
                int b = getMedian(x, y, BChannel);

                QColor newPixel = QColor(r,g,b);
                newImage->setPixel(x, y, newPixel.rgb());
            }
    }

    return newImage;
}

int NoiseMedian::getMedian(int x, int y, Channel channel)
{
    int radius = getParameter("radius").toInt();

    std::vector<float> windowAsVector;
    math::matrix<float> window = getWindow(x, y, radius*2+1, channel, RepeatEdge);

    int size = window.size();

    for (int i=0; i<window.rowsize(); ++i)
        for (int j=0; j<window.colsize(); ++j) {
            windowAsVector.push_back(window[i][j]);
        }

    std::sort(windowAsVector.begin(), windowAsVector.end());

    if ((size % 2) != 0) { // nieparzysty rozmiar
        return windowAsVector[(size/2)+1];
    }
    else {
        return (windowAsVector[(size/2)-1]+windowAsVector[(size/2)])/2;
    }
}

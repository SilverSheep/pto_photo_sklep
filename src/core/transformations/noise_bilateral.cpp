#include "noise_bilateral.h"

NoiseBilateral::NoiseBilateral(PNM* img) :
    Convolution(img)
{
}

NoiseBilateral::NoiseBilateral(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseBilateral::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    sigma_d = getParameter("sigma_d").toFloat();
    sigma_r = getParameter("sigma_r").toFloat();
    radius = sigma_d;

    if (image->format() == QImage::Format_Indexed8) {
        // Iterate over image space
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                newImage->setPixel(x, y, calcVal(x, y, LChannel));
            }
    }
    else {
        // Iterate over image space
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int r = calcVal(x, y, RChannel);
                int g = calcVal(x, y, GChannel);
                int b = calcVal(x, y, BChannel);

                QColor newPixel = QColor(r,g,b);
                newImage->setPixel(x, y, newPixel.rgb());
            }
    }

    return newImage;
}

int NoiseBilateral::calcVal(int x, int y, Channel channel)
{   
    QColor color = image->pixel(x,y);
    int a = color.value();
    math::matrix<float> window = getWindow(x, y, radius, channel, RepeatEdge);

    float dividend = 0;
    float divisor = 0;

    for (int i=0; i<window.rowsize(); ++i)
        for (int j=0; j<window.colsize(); ++j) {
            float a = colorCloseness(window[i][j], window[radius/2][radius/2]);
            float b = spatialCloseness(QPoint(i, j), QPoint(radius/2, radius/2));

            dividend += window[i][j] * a * b;
            divisor += a * b;
        }

    return dividend/divisor;
}

float NoiseBilateral::colorCloseness(int val1, int val2)
{
    return qExp(-(float)((val1-val2)*(val1-val2))/(2*sigma_r*sigma_r));
}

float NoiseBilateral::spatialCloseness(QPoint point1, QPoint point2)
{
    float a = (point1.x()-point2.x())*(point1.x()-point2.x());
    float b = (point1.y()-point2.y())*(point1.y()-point2.y());

    return qExp(-(a+b)/(2*sigma_d*sigma_d));
}

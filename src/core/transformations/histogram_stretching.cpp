#include "histogram_stretching.h"

#include "../histogram.h"

HistogramStretching::HistogramStretching(PNM* img) :
    Transformation(img)
{
}

HistogramStretching::HistogramStretching(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* HistogramStretching::transform()
{
    Histogram* histogram = image->getHistogram();

    PNM* newImage;

    if (image->format() == QImage::Format_Indexed8) {
        QHash<int, int>* channel = histogram->get(Histogram::LChannel);

        newImage = transformGreyscale(channel, image);
    }
    else {
        newImage = transformRGB(histogram, image);
    }

    return newImage;
}

PNM* HistogramStretching::transformGreyscale(QHash<int, int>* const channel, const PNM* const image) {
    int min = findMinForChannel(channel);
    int max = findMaxForChannel(channel);

    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QColor color = QColor::fromRgb(image->pixel(x,y));
            double newPixel = (double)(255*(color.value()-min))/(double)(max-min);
            newImage->setPixel(x, y, (uint)newPixel);
        }

    return newImage;
}

PNM* HistogramStretching::transformRGB(Histogram* const histogram, const PNM* const image) {
    QHash<int, int>* rChannel = histogram->get(Histogram::RChannel);
    QHash<int, int>* gChannel = histogram->get(Histogram::GChannel);
    QHash<int, int>* bChannel = histogram->get(Histogram::BChannel);

    int rMin = findMinForChannel(rChannel);
    int rMax = findMaxForChannel(rChannel);
    int gMin = findMinForChannel(gChannel);
    int gMax = findMaxForChannel(gChannel);
    int bMin = findMinForChannel(bChannel);
    int bMax = findMaxForChannel(bChannel);

    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QRgb pixel = image->pixel(x,y);

            int r = qRed(pixel);    // Get the 0-255 value of the R channel
            int g = qGreen(pixel);  // Get the 0-255 value of the G channel
            int b = qBlue(pixel);   // Get the 0-255 value of the B channel

            double newR = (double)(255*(r-rMin))/(double)(rMax-rMin);
            double newG = (double)(255*(g-gMin))/(double)(gMax-gMin);
            double newB = (double)(255*(b-bMin))/(double)(bMax-bMin);

            QColor newPixel = QColor((uint)newR, (uint)newG, (uint)newB);

            newImage->setPixel(x, y, newPixel.rgb());
        }
    return newImage;
}


int HistogramStretching::findMinForChannel(QHash<int, int>* const channel) {
    int result = 0;
    QMap<int, int>* map = new QMap<int, int>();

    hashToMap(channel, map);
    result = map->keys().first();

    delete map;
    return result;
}

int HistogramStretching::findMaxForChannel(QHash<int, int>* const channel) {
    int result = 0;
    QMap<int, int>* map = new QMap<int, int>();

    hashToMap(channel, map);
    result = map->keys().last();

    delete map;
    return result;
}

void HistogramStretching::hashToMap(QHash<int, int>* const hash, QMap<int, int>* const result) {
    QHash<int, int>::iterator i;

    for (i = hash->begin(); i != hash->end(); ++i) {
        result->insert(i.key(), i.value());
    }
}

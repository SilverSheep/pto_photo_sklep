#include "edge_roberts.h"

EdgeRoberts::EdgeRoberts(PNM* img) :
    EdgeGradient(img)
{
    prepareMatrices();
}

EdgeRoberts::EdgeRoberts(PNM* img, ImageViewer* iv) :
    EdgeGradient(img, iv)
{
    prepareMatrices();
}

void EdgeRoberts::prepareMatrices()
{
    float xValues[] = { 1, 0, 0,-1};
    float yValues[] = { 0, 1, -1, 0};

    g_x = math::matrix<float>(2, 2, xValues);
    g_y = math::matrix<float>(2, 2, yValues);

}

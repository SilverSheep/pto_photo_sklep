#ifndef HISTOGRAM_EQUALIZATION_H
#define HISTOGRAM_EQUALIZATION_H

#include "transformation.h"

class HistogramEqualization : public Transformation
{
public:
    HistogramEqualization(PNM*);
    HistogramEqualization(PNM*, ImageViewer*);

    virtual PNM* transform();
private:
    PNM* transformGreyscale(QHash<int, int>* const channel, const PNM* const image);
    PNM* transformRGB(Histogram* const histogram, const PNM* const image);

    void channelToScaledDistribution(QHash<int, int>* const channel, QMap<int, double>* const distribution, const double allPixels);
};


#endif // HISTOGRAM_EQUALIZATION_H

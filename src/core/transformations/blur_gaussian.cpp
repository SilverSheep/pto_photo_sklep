#include "blur_gaussian.h"

BlurGaussian::BlurGaussian(PNM* img) :
    Convolution(img)
{
}

BlurGaussian::BlurGaussian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* BlurGaussian::transform()
{
    emit message("Blurring...");

    int size = getParameter("size").toInt();
    radius = (size/2)+1;
    sigma = getParameter("sigma").toDouble();

    return convolute(getMask(size, Normalize), RepeatEdge);
}

math::matrix<float> BlurGaussian::getMask(int size, Mode)
{
    math::matrix<float> mask(size, size);

    int radius = size/2;

    for (int x=0; x<size; x++)
        for (int y=0; y<size; y++)
        {
            float a = getGauss(x-radius, y-radius, 1.0);
            mask[x][y] = a;
        }

    return mask;
}

float BlurGaussian::getGauss(int x, int y, float sigma)
{
    float dividend = qExp(-(x*x+y*y)/2*sigma*sigma);
    float divisor = 2 * M_PI * sigma * sigma;

    return dividend/divisor;
}


#include "histogram_equalization.h"

#include "../histogram.h"

HistogramEqualization::HistogramEqualization(PNM* img) :
    Transformation(img)
{
}

HistogramEqualization::HistogramEqualization(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* HistogramEqualization::transform()
{
    PNM* newImage;

    Histogram* histogram = image->getHistogram();

    if (image->format() == QImage::Format_Indexed8) {
        QHash<int, int>* channel = histogram->get(Histogram::LChannel);

        newImage = transformGreyscale(channel, image);
    }
    else {
        newImage = transformRGB(histogram, image);
    }

    return newImage;
}

PNM* HistogramEqualization::transformGreyscale(QHash<int, int>* const channel, const PNM* const image) {
    int width  = image->width();
    int height = image->height();
    double allPixels = width * height;

    PNM* newImage = new PNM(width, height, image->format());

    QMap<int,double>* distribution = new QMap<int,double>();

    channelToScaledDistribution(channel, distribution, allPixels);

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QColor color = QColor::fromRgb(image->pixel(x,y));
            uint newPixel = (distribution->value(color.value()));
            newImage->setPixel(x, y, newPixel);
        }


    delete distribution;
    return newImage;
}

PNM* HistogramEqualization::transformRGB(Histogram* const histogram, const PNM* const image) {
    int width  = image->width();
    int height = image->height();
    double allPixels = width * height;

    PNM* newImage = new PNM(width, height, image->format());

    QHash<int, int>* rChannel = histogram->get(Histogram::RChannel);
    QHash<int, int>* gChannel = histogram->get(Histogram::GChannel);
    QHash<int, int>* bChannel = histogram->get(Histogram::BChannel);

    QMap<int,double>* rDistribution = new QMap<int,double>();
    QMap<int,double>* gDistribution = new QMap<int,double>();
    QMap<int,double>* bDistribution = new QMap<int,double>();

    channelToScaledDistribution(rChannel, rDistribution, allPixels);
    channelToScaledDistribution(gChannel, gDistribution, allPixels);
    channelToScaledDistribution(bChannel, bDistribution, allPixels);

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QRgb pixel = image->pixel(x,y);

            int r = qRed(pixel);    // Get the 0-255 value of the R channel
            int g = qGreen(pixel);  // Get the 0-255 value of the G channel
            int b = qBlue(pixel);   // Get the 0-255 value of the B channel

            uint newR = (rDistribution->value(r));
            uint newG = (gDistribution->value(g));
            uint newB = (bDistribution->value(b));

            QColor newPixel = QColor((uint)newR, (uint)newG, (uint)newB);

            newImage->setPixel(x, y, newPixel.rgb());
        }

    delete rDistribution;
    delete gDistribution;
    delete bDistribution;
    return newImage;
}

void HistogramEqualization::channelToScaledDistribution(QHash<int, int>* const channel, QMap<int, double>* const distribution, const double allPixels) {
    QHash<int, int>::iterator i;

    for (i = channel->begin(); i != channel->end(); ++i) {
        distribution->insert(i.key(), i.value()/allPixels);
    }

    double new_value = 0;

    QMap<int, double>::iterator it;

    for (it = distribution->begin(); it != distribution->end(); ++it) {
        new_value += it.value();
        distribution->insert(it.key(), 255*new_value);
    }
}

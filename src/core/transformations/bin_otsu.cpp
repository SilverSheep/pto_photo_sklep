#include "bin_otsu.h"

#include "histogram_equalization.h"
#include "../histogram.h"

BinarizationOtsu::BinarizationOtsu(PNM* img) :
    Transformation(img)
{
}

BinarizationOtsu::BinarizationOtsu(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationOtsu::transform()
{
    int width = image->width();
    int height = image->height();

    int totalPixels = width*height;

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    float eta[PIXEL_VAL_MAX];

    // converting to gray
    if (image->format() == QImage::Format_RGB32) {
        ConversionGrayscale cg(image);
        image = cg.transform();
    }

    // equlizing histogram
    HistogramEqualization he(image);
    image = he.transform();

    // converting histogram to array
    Histogram* histogram = image->getHistogram();

    QHash<int, int>::iterator i;
    QHash<int, int>* hist = histogram->get(Histogram::LChannel);

    int histogramArray[PIXEL_VAL_MAX];

    for (int i=0; i<PIXEL_VAL_MAX; ++i) {
        histogramArray[i] = 0;
    }

    for (i = hist->begin(); i != hist->end(); ++i) {
        histogramArray[i.key()] = i.value();
    }

    // calculating eta
    for (int i=0; i<PIXEL_VAL_MAX; ++i) {
        float p0 = P0(histogramArray, totalPixels, i);
        float p1 = P1(histogramArray, totalPixels, i);
        float u0 = mi0(histogramArray, i);
        float u1 = mi1(histogramArray, i);

        eta[i] = p0 * p1 * (u1 - u0) * (u1 - u0);
    }


    // looking for maximum eta
    float maxEta = eta[0];
    int threshold = 0;

    for (int i=1; i<PIXEL_VAL_MAX; ++i)
        if (eta[i] > maxEta) {
            maxEta = eta[i];
            threshold = i;
        }

    // binarization
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++) {
            QColor pixel = image->pixel(x,y);
            int newValue = pixel.value() < threshold ? 0 : 1;
            newImage->setPixel(x, y, newValue);
        }

    return newImage;
}

float BinarizationOtsu::P0(int* histogram, float totalPixels, int threshold) {
    float sum = 0;

    for (int i=0; i<threshold; ++i) {
        sum += histogram[i];
    }

    return sum/totalPixels;
}

float BinarizationOtsu::P1(int* histogram, float totalPixels, int threshold) {
    float sum = 0;

    for (int i=threshold; i<PIXEL_VAL_MAX; ++i) {
        sum += histogram[i];
    }

    return sum/totalPixels;
}

float BinarizationOtsu::mi0(int* histogram, int threshold) {
    float sum = 0;
    int sumMultiplied = 0;

    for (int i=0; i<threshold; ++i) {
        sum += histogram[i];
        sumMultiplied += histogram[i]*i;
    }

    return sum == 0 ? sum : sumMultiplied/sum;
}

float BinarizationOtsu::mi1(int* histogram, int threshold) {
    float sum = 0;
    int sumMultiplied = 0;

    for (int i=threshold; i<PIXEL_VAL_MAX; ++i) {
        sum += histogram[i];
        sumMultiplied += histogram[i]*i;
    }

    return sum == 0 ? sum : sumMultiplied/sum;
}

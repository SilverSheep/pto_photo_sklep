#include "convolution.h"

/** Overloaded constructor */
Convolution::Convolution(PNM* img) :
    Transformation(img)
{
}

Convolution::Convolution(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

/** Returns a convoluted form of the image */
PNM* Convolution::transform()
{
    return convolute(getMask(3, Normalize), RepeatEdge);
}

/** Returns a sizeXsize matrix with the center point equal 1.0 */
math::matrix<float> Convolution::getMask(int size, Mode mode = Normalize)
{
    math::matrix<float> mask(size, size);

    int radius = size/2;

    for (int i=0; i<size; i++)
        for (int j=0; j<size; j++)
            mask[i][j] = (i==radius && j==radius); // 1 for center

    return mask;
}

/** Does the convolution process for all pixels using the given mask. */
PNM* Convolution::convolute(math::matrix<float> mask, Mode mode = RepeatEdge)
{
    int size = mask.rowno(),
            maskWeight = sum(mask);

    math::matrix<float> maskReflection = reflection(mask);

    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8) {
        // Iterate over image space
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                math::matrix<float> accumulator(size,size);
                math::matrix<float> window = getWindow(x, y, size, LChannel, mode);

                for (int i=0; i<size; ++i)
                    for (int j=0; j<size; ++j) {
                        int a = window[i][j];
                        int b = maskReflection[i][j];
                        accumulator[i][j] = window[i][j] * maskReflection[i][j];
                    }

                int accumulatorSum = sum(accumulator);
                accumulatorSum /= (maskWeight != 0) ? maskWeight : 1;

                accumulatorSum = qMax(qMin(255, accumulatorSum), 0);

                newImage->setPixel(x, y, accumulatorSum);
            }

    }
    else { //if (image->format() == QImage::Format_RGB32)

        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                math::matrix<float> RWindow = getWindow(x, y, size, RChannel, mode);
                math::matrix<float> GWindow = getWindow(x, y, size, GChannel, mode);
                math::matrix<float> BWindow = getWindow(x, y, size, BChannel, mode);

                math::matrix<float> RAccumulator(size,size);
                math::matrix<float> GAccumulator(size,size);
                math::matrix<float> BAccumulator(size,size);

                for (int i=0; i<size; ++i)
                    for (int j=0; j<size; ++j) {
                        RAccumulator[i][j] = RWindow[i][j] * maskReflection[i][j];
                        GAccumulator[i][j] = GWindow[i][j] * maskReflection[i][j];
                        BAccumulator[i][j] = BWindow[i][j] * maskReflection[i][j];
                    }

                int RAccumulatorSum = sum(RAccumulator);
                int GAccumulatorSum = sum(GAccumulator);
                int BAccumulatorSum = sum(BAccumulator);

                RAccumulatorSum /= (maskWeight != 0) ? maskWeight : 1;
                GAccumulatorSum /= (maskWeight != 0) ? maskWeight : 1;
                BAccumulatorSum /= (maskWeight != 0) ? maskWeight : 1;

                RAccumulatorSum = qMax(qMin(255, RAccumulatorSum), 0);
                GAccumulatorSum = qMax(qMin(255, GAccumulatorSum), 0);
                BAccumulatorSum = qMax(qMin(255, BAccumulatorSum), 0);

                QColor newPixel = QColor(RAccumulatorSum,GAccumulatorSum,BAccumulatorSum);
                newImage->setPixel(x, y, newPixel.rgb());
            }
    }

    return newImage;
}

/** Joins to matrices by multiplying the A[i,j] with B[i,j].
  * Warning! Both Matrices must be squares with the same size!
  */
const math::matrix<float> Convolution::join(math::matrix<float> A, math::matrix<float> B)
{
    int size = A.rowno();
    math::matrix<float> C(size, size);

    for (int i=0; i<size; i++)
        for (int j=0; j<size; j++)
            C[i][j] = A[i][j] * B[i][j];

    return C;
}

/** Sums all of the matrixes elements */
const float Convolution::sum(const math::matrix<float> A)
{
    float sum = 0.0;
    int size = A.rowno();

    for (int i=0; i<size; i++)
        for (int j=0; j<size; j++)
            sum += A[i][j];

    return sum;
}


/** Returns reflected version of a matrix */
const math::matrix<float> Convolution::reflection(const math::matrix<float> A)
{
    int size = A.rowno();
    math::matrix<float> C(size, size);

    for (int i=0; i<size; i++)
        for (int j=0; j<size; j++) {
            C[i][j] = A[size-1-i][size-1-j];
        }

    return C;
}

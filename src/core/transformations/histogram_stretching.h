#ifndef HISTOGRAM_STRETCHING_H
#define HISTOGRAM_STRETCHING_H

#include "transformation.h"

class HistogramStretching : public Transformation
{
public:
    HistogramStretching(PNM*);
    HistogramStretching(PNM*, ImageViewer* iv);

    virtual PNM* transform();

private:
    PNM* transformGreyscale(QHash<int, int>* const channel, const PNM* const image);
    PNM* transformRGB(Histogram* const histogram, const PNM* const image);

    int findMinForChannel(QHash<int, int>* const channel);
    int findMaxForChannel(QHash<int, int>* const channel);

    void hashToMap(QHash<int, int>* const hash, QMap<int, int>* const result);
};

#endif // HISTOGRAM_STRETCHING_H

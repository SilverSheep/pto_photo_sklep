#include "bin_gradient.h"

BinarizationGradient::BinarizationGradient(PNM* img) :
    Transformation(img)
{
}

BinarizationGradient::BinarizationGradient(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationGradient::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    // converting to gray
    if (image->format() == QImage::Format_RGB32) {
        ConversionGrayscale cg(image);
        image = cg.transform();
    }

    float sum = 0;
    float sumMultiplied = 0;
    float threshold;

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            QColor colorGX1 = QColor::fromRgb(getPixel(x+1, y, RepeatEdge));
            float gX1 = colorGX1.value();
            QColor colorGX2 = QColor::fromRgb(getPixel(x-1, y, RepeatEdge));
            float gX2 = colorGX2.value();
            QColor colorGY1 = QColor::fromRgb(getPixel(x, y+1, RepeatEdge));
            float gY1 = colorGY1.value();
            QColor colorGY2 = QColor::fromRgb(getPixel(x, y-1, RepeatEdge));
            float gY2 = colorGY2.value();

            float gX = gX1 - gX2;
            float gY = gY1 - gY2;

            QColor color = getPixel(x, y, RepeatEdge);
            sumMultiplied += qMax(gX, gY) * color.value();
            sum += qMax(gX, gY);
        }

    threshold = sum == 0 ? 0 : sumMultiplied / sum;

    // binarization
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++) {
            QColor pixel = image->pixel(x,y);
            int newValue = pixel.value() < threshold ? 0 : 1;
            newImage->setPixel(x, y, newValue);
        }

    return newImage;
}



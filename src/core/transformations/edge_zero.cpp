#include "edge_zero.h"

#include "edge_laplacian_of_gauss.h"

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img) :
    Convolution(img)
{
}

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* EdgeZeroCrossing::transform()
{
    int width = image->width(),
            height = image->height();

    int    size  = getParameter("size").toInt();
    double sigma = getParameter("sigma").toDouble();
    int    t     = getParameter("threshold").toInt();
    Mode mode = RepeatEdge;

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);

    math::matrix<float> mask = getMask(size, Normalize);
    PNM* laplacian = convolute(mask, mode);

    int v0 = 128;

    if (image->format() == QImage::Format_RGB32) {
        ConversionGrayscale cg(image);
        image = cg.transform();
    }

    // Iterate over image space
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
            math::matrix<float> window = getWindow(x, y, size, LChannel, RepeatEdge);

            int max = 0.0;
            int min = PIXEL_VAL_MAX + 1;

            for (int x=0; x<size; x++)
                for (int y=0; y<size; y++) {
                    if (window[x][y] > max) {
                        max = window[x][y];
                    }
                    else if (window[x][y] < min){
                        min = window[x][y];
                    }
                }

            QColor color = laplacian->pixel(x, y);

            if (min < (v0 - t) && max > (v0-t)) {
                newImage->setPixel(x, y, color.value());
            }
            else {
                newImage->setPixel(x, y, 0);
            }
        }

    delete laplacian;

    return newImage;
}


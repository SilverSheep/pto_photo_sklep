#include "edge_sobel.h"

EdgeSobel::EdgeSobel(PNM* img, ImageViewer* iv) :
    EdgeGradient(img, iv)
{
    prepareMatrices();
}

EdgeSobel::EdgeSobel(PNM* img) :
    EdgeGradient(img)
{
    prepareMatrices();
}

void EdgeSobel::prepareMatrices()
{
    float xValues[] = { -1, 0, 1, -2, 0, 2, -1, 0, 1};
    float yValues[] = { -1, -2, -1, 0, 0, 0, 1, 2, 1};

    g_x = math::matrix<float>(3, 3, yValues);
    g_y = math::matrix<float>(3, 3, xValues);
}

math::matrix<float>* EdgeSobel::rawHorizontalDetection()
{
    math::matrix<float>* x_gradient = new math::matrix<float>(this->image->width(), this->image->height());

    int size = 3;
    math::matrix<float> A(size, size);

    // Iterate over image space
    for (int x=0; x<image->width(); x++)
        for (int y=0; y<image->height(); y++)
        {
                math::matrix<float> window = getWindow(x, y, size, LChannel, RepeatEdge);

                A = join(g_x, window);

                float sum = 0;
                for (int i=0; i<size; i++)
                    for (int j=0; j<size; j++)
                        sum += A[i][j];

                (*x_gradient)[x][y] = sum;
        }

    return x_gradient;
}

math::matrix<float>* EdgeSobel::rawVerticalDetection()
{
    math::matrix<float>* y_gradient = new  math::matrix<float>(this->image->width(), this->image->height());

    int size = 3;
    math::matrix<float> A(size, size);

    // Iterate over image space
    for (int x=0; x<image->width(); x++)
        for (int y=0; y<image->height(); y++)
        {
                math::matrix<float> window = getWindow(x, y, size, LChannel, RepeatEdge);

                A = join(g_x, window);

                float sum = 0;
                for (int i=0; i<size; i++)
                    for (int j=0; j<size; j++)
                        sum += A[i][j];

                (*y_gradient)[x][y] = sum;
        }

    return y_gradient;
}

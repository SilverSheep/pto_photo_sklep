#ifndef BIN_OTSU_H
#define BIN_OTSU_H

#include "transformation.h"
#include "histogram_equalization.h"

class BinarizationOtsu : public Transformation
{
public:
    BinarizationOtsu(PNM*);
    BinarizationOtsu(PNM*, ImageViewer*);

    virtual PNM* transform();

private:
    float P0(int* histogram, float totalPixels, int threshold);
    float P1(int* histogram, float totalPixels, int threshold);

    float mi0(int* histogram, int threshold);
    float mi1(int* histogram, int threshold);

    //array<int, PIXEL_VAL_MAX> histogramToArray(Histogram* const histogram);
};


#endif // BIN_OTSU_H
